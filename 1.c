#include <stdio.h>
#define MAX_CHAR_SIZE 1000

int main()
{
    FILE *fpW, *fpR, *fpA;
    char content[MAX_CHAR_SIZE];

    fpW = fopen("assignment9.txt", "w");
    fprintf(fpW, "UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fpW);

    fpR = fopen("assignment9.txt", "r");
    while(!feof(fpR))
    {
        fgets(content, MAX_CHAR_SIZE, fpR);
        puts(content);
    }
    fclose(fpR);

    fpA = fopen("assignment9.txt", "a");
    fprintf(fpA, "\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fpA);

    return 0;
}
